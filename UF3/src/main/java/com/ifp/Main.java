package com.ifp;

import com.ifp.ejercicio6.Write_Personas_DOM_XML;
import com.ifp.ejercicio7.Read_DOM_XML;
import com.ifp.ejercicio8y9y10.Read_SAX_XML;
import com.ifp.ejercicio8y9y10.Write_Alumnos_DOM_XML;
import com.ifp.shared.Menu;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * @author Daniel Díez Miguel
 */
public class Main {
    /**
     * Método main el cual llama otros métodos dependiendo de la respuesta que llegue en el menú
     *
     * @param args
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static void main(String[] args) throws ParserConfigurationException, TransformerException, IOException, SAXException {

        int i = 1;
        while (i != 0) {
            switch (new Menu().menuFeature()) {
                case 1 -> new Write_Personas_DOM_XML().loopXML();
                case 2 -> new Read_DOM_XML().readDomXml();
                case 3 -> new Write_Alumnos_DOM_XML().loopXML();
                case 4, 5 -> new Read_SAX_XML().readSaxXML();
                default -> i = 0;
            }
        }
    }
}