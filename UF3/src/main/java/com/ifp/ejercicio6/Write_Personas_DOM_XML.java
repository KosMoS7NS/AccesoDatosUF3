package com.ifp.ejercicio6;

import com.ifp.shared.Menu;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Scanner;

import static com.ifp.shared.Menu.*;
import static java.lang.System.in;
import static javax.xml.parsers.DocumentBuilderFactory.newInstance;

/**
 * Clase con los métodos de writeXML() y loopXML()
 */
public class Write_Personas_DOM_XML {

    /**
     * Método en el cual se escribe un XML a partir de los datos proporcionados por el usuario
     *
     * @param document
     * @param personas
     * @param i
     * @return i
     * @throws TransformerException
     */
    private static int writeXml(Document document, Element personas, int i) throws TransformerException {

        Scanner scanner = new Scanner(in);
        new Menu().dataXML(scanner, RESPUESTA);

        Element persona = document.createElement("persona");

        Element persona_nombre = document.createElement("nombre");
        Text persona_nombre_text = document.createTextNode(NOMBRE);

        persona_nombre.appendChild(persona_nombre_text);
        persona.appendChild(persona_nombre);

        Element persona_edad = document.createElement("edad");
        Text persona_edad_text = document.createTextNode(String.valueOf(EDAD));

        persona_edad.appendChild(persona_edad_text);
        persona.appendChild(persona_edad);

        Element persona_nacionalidad = document.createElement("nacionalidad");
        Text persona_nacionalidad_text = document.createTextNode(NACIONALIDAD);

        persona_nacionalidad.appendChild(persona_nacionalidad_text);
        persona.appendChild(persona_nacionalidad);
        personas.appendChild(persona);

        document.getDocumentElement()
                .appendChild(personas);

        Source source = new DOMSource(document);
        Result result = new StreamResult(new File("personas.xml"));

        TransformerFactory.newInstance()
                .newTransformer()
                .transform(source, result);

        i++;
        return i;
    }

    /**
     * Método en el cual se realiza un loop para la escritura del XML
     *
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void loopXML() throws ParserConfigurationException, TransformerException {
        DOMImplementation implementation = newInstance()
                .newDocumentBuilder()
                .getDOMImplementation();

        Document document = implementation.createDocument(null, "personas", null);
        document.setXmlVersion("1.0");

        Element personas = document.createElement("personas");

        int i = 0;
        while (i < 3) i = writeXml(document, personas, i);
        System.out.println("Fichero XML creado");
    }
}

