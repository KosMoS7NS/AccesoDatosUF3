package com.ifp.ejercicio7;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static javax.xml.parsers.DocumentBuilderFactory.newInstance;
import static org.w3c.dom.Node.ELEMENT_NODE;

/**
 * Clase con los métodos filterElementNode() y readDomXml()
 */
public class Read_DOM_XML {

    /**
     * Método en el cual se filtra por ELEMENT_NODE y se recoge el contenido de los elementos
     *
     * @param persona
     * @param i
     */
    private static void filterElementNode(NodeList persona, int i) {
        Node persona_node = persona.item(i);
        if (persona_node.getNodeType() == ELEMENT_NODE) {
            Element elemento = (Element) persona_node;

            System.out.printf("NOMBRE: %s %n", elemento
                    .getElementsByTagName("nombre")
                    .item(0)
                    .getTextContent());

            System.out.printf("EDAD: %s %n", elemento
                    .getElementsByTagName("edad")
                    .item(0)
                    .getTextContent());

            System.out.printf("NACIONALIDAD: %s %n %n", elemento
                    .getElementsByTagName("nacionalidad")
                    .item(0)
                    .getTextContent());
        }
    }

    /**
     * Método en el cual se obtiene el documento xml y se realiza un bucle del método anterior pasando como parámetros
     * el nombre del nodo de persona y el iterador
     *
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void readDomXml() throws ParserConfigurationException, IOException, SAXException {
        Document document = newInstance()
                .newDocumentBuilder()
                .parse(new File("personas.xml"));

        document.getDocumentElement().normalize();
        NodeList persona = document.getElementsByTagName("persona");

        for (int i = 0; i < persona.getLength(); i++) filterElementNode(persona, i);

    }
}
