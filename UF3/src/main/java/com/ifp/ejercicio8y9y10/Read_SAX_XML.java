package com.ifp.ejercicio8y9y10;

import com.ifp.shared.Menu;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.ifp.shared.Menu.RESPUESTA;
import static com.ifp.shared.Menu.XML;
import static java.lang.System.in;

/**
 * Clase con los métodos de DefalutHandler
 */
public class Read_SAX_XML extends DefaultHandler {
    public Read_SAX_XML() {
        super();
    }

    /**
     * Lectura del XML con y sin lista dependiendo de la respuesta del usuario
     * @param car
     */
    private static void dataXml(String car) {
        List list = new ArrayList<>();
        if (!car.isBlank()) list.add("Carácteres: " + car);

        if (RESPUESTA == 4) System.out.printf("Carácteres: %s %n", car);
        else list.forEach(System.out::println);
    }

    /**
     * Comienzo del documento
     *
     * @throws SAXException
     */
    @Override
    public void startDocument() throws SAXException {
        System.out.println(" Comienzo del documento XML");
    }

    /**
     * Fin del documento
     *
     * @throws SAXException
     */
    @Override
    public void endDocument() throws SAXException {
        System.out.println("\n Fin del documento XML");
    }

    /**
     * Principio del elemento
     *
     * @param uri        The Namespace URI, or the empty string if the
     *                   element has no Namespace URI or if Namespace
     *                   processing is not being performed.
     * @param localName  The local name (without prefix), or the
     *                   empty string if Namespace processing is not being
     *                   performed.
     * @param qName      The qualified name (with prefix), or the
     *                   empty string if qualified names are not available.
     * @param attributes The attributes attached to the element.  If
     *                   there are no attributes, it shall be an empty
     *                   Attributes object.
     * @throws SAXException
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.printf("\n Principio del elemento: %s %n ", qName);
    }

    /**
     * Fin del elemento
     *
     * @param uri       The Namespace URI, or the empty string if the
     *                  element has no Namespace URI or if Namespace
     *                  processing is not being performed.
     * @param localName The local name (without prefix), or the
     *                  empty string if Namespace processing is not being
     *                  performed.
     * @param qName     The qualified name (with prefix), or the
     *                  empty string if qualified names are not available.
     * @throws SAXException
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.printf(" \n Fin del elemento: %s %n ", qName);
    }

    /**
     * Texto del nodo
     *
     * @param ch     The characters.
     * @param start  The start position in the character array.
     * @param length The number of characters to use from the
     *               character array.
     * @throws SAXException
     */
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String car = new String(ch, start, length);
        car = car.replaceAll("[\t\n]", "");

        dataXml(car);

    }

    /**
     * Lectura de XML con SAX
     *
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public void readSaxXML() throws ParserConfigurationException, SAXException, IOException {
        Scanner scanner = new Scanner(in);
        Menu.dataXML(scanner, RESPUESTA);
        SAXParser saxParser = SAXParserFactory
                .newInstance()
                .newSAXParser();

        Read_SAX_XML read_sax_xml = new Read_SAX_XML();
        InputSource fileXML = new InputSource(XML);
        saxParser.parse(fileXML, read_sax_xml);
    }
}
