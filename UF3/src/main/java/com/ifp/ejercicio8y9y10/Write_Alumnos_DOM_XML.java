package com.ifp.ejercicio8y9y10;

import com.ifp.shared.Menu;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Scanner;

import static com.ifp.shared.Menu.*;
import static java.lang.System.in;
import static javax.xml.parsers.DocumentBuilderFactory.newInstance;

/**
 * Clase con los métodos de writeXML() y loopXML()
 */
public class Write_Alumnos_DOM_XML {

    /**
     * Método en el cual se escribe un XML a partir de los datos proporcionados por el usuario
     *
     * @param document
     * @param personas
     * @param i
     * @return i
     * @throws TransformerException
     */
    private static int writeXml(Document document, Element personas, int i) throws TransformerException {

        Scanner scanner = new Scanner(in);
        new Menu().dataXML(scanner, RESPUESTA);

        Element persona = document.createElement("alumno");

        Element persona_nombre = document.createElement("nombre");
        Text persona_nombre_text = document.createTextNode(NOMBRE);

        persona_nombre.appendChild(persona_nombre_text);
        persona.appendChild(persona_nombre);

        Element persona_apellido = document.createElement("apellido");
        Text persona_apellido_text = document.createTextNode(APELLIDO);

        persona_apellido.appendChild(persona_apellido_text);
        persona.appendChild(persona_apellido);

        Element persona_usuario = document.createElement("usuario");
        Text persona_usuario_text = document.createTextNode(USUARIO);

        persona_usuario.appendChild(persona_usuario_text);
        persona.appendChild(persona_usuario);

        Element persona_password = document.createElement("password");
        Text persona_password_text = document.createTextNode(PASSWORD);

        persona_password.appendChild(persona_password_text);
        persona.appendChild(persona_password);

        Element persona_idioma = document.createElement("idioma");
        Text persona_idioma_text = document.createTextNode(IDIOMA);

        persona_idioma.appendChild(persona_idioma_text);
        persona.appendChild(persona_idioma);

        personas.appendChild(persona);
        document.getDocumentElement()
                .appendChild(personas);

        Source source = new DOMSource(document);
        Result result = new StreamResult(new File("alumnos.xml"));

        TransformerFactory.newInstance()
                .newTransformer()
                .transform(source, result);

        i++;
        return i;
    }

    /**
     * Método en el cual se realiza un loop para la escritura del XML
     *
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void loopXML() throws ParserConfigurationException, TransformerException {
        DOMImplementation implementation = newInstance()
                .newDocumentBuilder()
                .getDOMImplementation();

        Document document = implementation.createDocument(null, "alumnos", null);
        document.setXmlVersion("1.0");

        Element personas = document.createElement("alumnos");

        int i = 0;
        while (i < 3) i = writeXml(document, personas, i);
        System.out.println("Fichero XML creado");
    }
}

