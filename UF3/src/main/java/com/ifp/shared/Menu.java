package com.ifp.shared;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.in;
import static java.lang.System.out;

/**
 * Clase con variables estáticas que se utilizan en el método de escritura XML.
 * Recoge los métodos dataXML() y menuFeature()
 */
public class Menu {

    public static String NOMBRE;
    public static String EDAD;
    public static String NACIONALIDAD;
    public static String APELLIDO;
    public static String USUARIO;
    public static String PASSWORD;
    public static String IDIOMA;
    public static String XML;
    public static int RESPUESTA;

    /**
     * Método cuya funcinalidad es recoger el nombre, la edad y la nacionalidad
     *
     * @param scanner
     * @param respuesta
     */
    public static void dataXML(Scanner scanner, int respuesta) {
        switch (respuesta) {
            case 1:
                out.println("Introduce el nombre");
                NOMBRE = scanner.next();

                out.println("Introduce la edad");
                EDAD = scanner.next();

                out.println("Introduce la nacionalidad");
                NACIONALIDAD = scanner.next();
                break;

            case 2:
                break;

            case 3:
                out.println("Introduce el nombre");
                NOMBRE = scanner.next();

                out.println("Introduce el apellido");
                APELLIDO = scanner.next();

                out.println("Introduce el usuario");
                USUARIO = scanner.next();

                out.println("Introduce la password");
                PASSWORD = scanner.next();

                out.println("Introduce el idioma");
                IDIOMA = scanner.next();
                break;

            case 4:
                out.println("Introduce el nombre del fichero XML a leer: ");
                XML = scanner.next();
                break;

            case 5:
                out.println("Introduce el nombre del fichero XML a leer: ");
                XML = scanner.next();
                break;

        }
    }

    /**
     * Método cuya funcionalidad es mostrar el menú
     *
     * @return respuesta
     */
    public int menuFeature() {
        Scanner scanner = new Scanner(in);

        List menu_principal = Arrays.asList(
                "|================================|",
                "|1) Crear personas DOM XML       |",
                "|2) Leer XML DOM                 |",
                "|3) Crear alumnos DOM XML        |",
                "|4) Leer alumnos SAX             |",
                "|5) Leer alumnos SAX Lista       |",
                "|0) Salir                        |",
                "|================================|");

        menu_principal.forEach(out::println);

        out.println("Introduce una respuesta: ");
        RESPUESTA = scanner.nextInt();

        RESPUESTA = RESPUESTA < 1 || RESPUESTA > 5 ? 0 : RESPUESTA;

        return RESPUESTA;
    }
}
